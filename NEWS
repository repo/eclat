Eclat NEWS -- history of user-visible changes. 2023-10-26
See the end of file for copying conditions.

Please send Eclat bug reports to <bug-eclat@gnu.org.ua>

Version 2.1.1, 2023-10-26

* Fix compilation with gcc 12.2.0

Version 2.1, 2022-03-03

* Implement the terminate-instances command.
* Implement MoveAddressToVpc (addr2vpc) and RestoreAddressToClassic (addr2ec2).
* Fix rmaddr -v.
* Document chvol and lschvol commands.
* Implement ModifyVolume and DescribeVolumesModifications.


Version 2.0, 2018-03-16

* Exponential backoff with jitter

If AWS responds with a RequestLimitExceeded code, eclat retries the
request using exponential backoff with jitter algorithm. The algorithm
is controlled by two values: max-retry-interval and total-retry-timeout.
When the RequestLimitExceeded error is returned, eclat will sleep for
2 seconds and then retry the request. For each subsequent
RequestLimitExceeded error, it will calculate the timeout using the
following formula:

   t = rand(0, min(M, 2 ** N)) + 1

where N is the attempt number, M is the value of max-retry-interval
parameter, 'rand(a,b)' selects the integer random number X such that
0 <= X <= b, and '**' denotes the power operator. The attempts to resend
the request will continue until either a response other than
RequestLimitExceeded is received (be it a response to the query or
another error response), or the total time spent in the retry loop
becomes equal to or greater than total-retry-timeout, whichever occurs
first.

* VPC Support

The following new commands provide full support for manipulating
the EC2 VPC objects:

 lsvpc	        describe-vpcs
 lsvpcattr      describe-vpc-attribute
 mkvpc          create-vpc
 setvpcattr     modify-vpc-attribute
 rmvpc          delete-vpc

* Route table support

The following new commands provide full support for manipulating
the EC2 Route Table objects:

 assocrtab      AssociateRouteTable
 mkrtab         CreateRouteTable
 rmrtab         DeleteRouteTable
 lsrtab         DescribeRouteTables
 disasrtab      DisassociateRouteTable

* Route support

The route subcommand allows you to add, delete and modify routes in
route tables.

** Create route:

 eclat route rtb-12345678 add CIDR gw ID

** Delete route:

 eclat route rtb-12345678 del CIDR

** ReplaceRoute

 eclat route rtb-12345678 repl CIDR gw ID
 
* Subnet support

The following new commands provide full support for manipulating
the EC2 Subnet objects:

 mksubnet       create-subnet 
 rmsubnet       delete-subnet
 lssubnet       describe-subnets
 setsubnetattr  modify-subnet-attribute

* Internet Gateway support
 
The following new commands provide full support for manipulating
the EC2 Internet Gateway objects:

 lsigw          describe-internet-gateways
 mkigw          create-internet-gateway
 rmigw          delete-internet-gateway
 atigw          attach-internet-gateway
 deigw          detach-internet-gateway

Version 1.1, 2015-02-26

* New commands:

 lsaattr   describe-image-attribute
 setaattr  modify-image-attribute

* New options:

 -A, --add-parameter=NAME=VALUE

Adds parameter NAME with the given VALUE to the generated AWS request.
This option is intended for testing new AWS functionality for which
eclat does not yet provide native support.

  -p, --check-permissions
  
Checks if you have the required permissions for the action, without
actually making the request.

* forcibly stop the instance

eclat stop now supports -f (--force) option, which stops the instance
forcibly.

* Implement signature version 4 signing

The signature version 4 signing process is now the default.  Version
2 can enabled by using the following statement in the eclat
configuration file:

  signature-version 2;

* POST support

POST HTTP method is supported.  It is enabled by the following
configuration statement:

  http-method post;
  
* If availability region is not supplied, it is read from the instance store.

* Authentication providers

Support for different authentication providers is introduced.
Authentication provider is a service that supplies AWS access key ID and
secret key.  It is configured by the "authentication-provider"
statement in the configuration file.  The syntax is:

  authentication-provider TYPE [ARG];

TYPE can be one of:

- file
Credentials are obtained from a disk file named by the second
argument.  The statement

  authentication-provider file NAME

is equivalent to

  access-file NAME

of eclat 1.0 and prior.  The "access-file" statement is retained for
backward compatibility.

- instance-store
Credentials are obtained from the instance store.  Second argument
is optional.  If present, it should be the name of the IAM role the
instance is launched with.

* IAM support

If authentication provider is set to "instance-store", its argument
specifies IAM role name.  The authentication credentials are then
taken from the instance store.

* New program: ispeek

Lists content of the instance store.

* Setting UserData instance attribute.

The setiattr (modify-instance-attribute) command automatically encodes
the value of UserData attribute in base64.  A command line option is
provided to read the value from a file.

* lsistat

The default for lsistat (describe-instance-status) command has been
changed to display all instances, no matter their status.  Use the
-r (--running) option to get prior behavior.  The -a (--all) option
has been retained for backward compatibility.


Version 1.0, 2013-12-20

First official release.  Implements a basic set of commands.


Version 0.1, 2012-10-23

First alpha release.

=========================================================================
Copyright information:

Copyright (C) 2012-2023 Sergey Poznyakoff

   Permission is granted to anyone to make or distribute verbatim copies
   of this document as received, in any medium, provided that the
   copyright notice and this permission notice are preserved,
   thus giving the recipient permission to redistribute in turn.

   Permission is granted to distribute modified versions
   of this document, or of portions of it,
   under the above conditions, provided also that they
   carry prominent notices stating who last changed them.

Local variables:
mode: outline
paragraph-separate: "[	]*$"
eval: (add-hook 'write-file-hooks 'time-stamp)
time-stamp-start: "changes. "
time-stamp-format: "%:y-%02m-%02d"
time-stamp-end: "\n"
end:

