/* This file is part of Eclat.
   Copyright (C) 2013-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

if (.RunInstancesResponse) {
  let var = .RunInstancesResponse;
  print("Reservation ID: ", var.reservationId, "\n");
  print("Owner ID: ", var.ownerId, "\n");
  if (var.groupSet) {
    print("Groups:\n");
    for (grp in var.groupSet.item)
      print("\t", grp.groupId,"\t", grp.groupName, "\n");
  }
  if (var.instancesSet) {
    // print("Instances:\n");
    for (inst in var.instancesSet.item) {
      print("\nInstance: ", inst.instanceId, "\n");
      print("\tImage ID: ", inst.imageId, "\n");
      print("\tState: ", inst.instanceState.name, "\n");
      print("\tKernel ID: ", inst.kernelId, "\n");
      print("\tArchitecture: ", inst.architecture, "\n");
      print("\tPrivate DNS: ", inst.privateDnsName, "\n");
      print("\tPublic DNS: ", inst.dnsName, "\n");
      print("\tKey: ", inst.keyName, "\n");
      print("\tType: ", inst.instanceType, "\n");
      print("\tLaunch Time: ", inst.launchTime, "\n");
      print("\tAvailability Zone: ", inst.placement.availabilityZone, "\n");
      print("\tGroup Name: ", inst.placement.groupName, "\n");
      print("\tTenancy: ", inst.placement.tenancy, "\n");
      print("\tPrivate IP: ", inst.privateIpAddress, "\n");
      print("\tPublic IP: ", inst.ipAddress, "\n");
      if (inst.groupSet.item) {
        print("\tGroups:\n");
        for (grp in inst.groupSet.item)
          print("\t\t", inst.groupSet.item.groupId, " -- ", inst.groupSet.item.groupName, "\n");
      }
      print("\tRoot Device Type: ", inst.rootDeviceType, "\n");
      print("\tRoot Device Name: ", inst.rootDeviceName, "\n");
      print("\tDevice mapping:\n");
      for (dev in inst.blockDeviceMapping.item) {
        print("\t\t", dev.deviceName, " ", dev.ebs.volumeId, " ",
              dev.ebs.status, " ", dev.ebs.attachTime, " ",
      	dev.ebs.deleteOnTermination, "\n");
      }
      print("\tVirtualization: ", inst.virtualizationType, "\n");
      print("\tTag Set:\n");
      for (tag in inst.tagSet.item) {
        print("\t\t", tag.key, "=", tag.value, "\n");
      }
      print("\tHypervisor: ", inst.hypervisor, "\n");
      // FIXME: networkInterfaceSet
      print("\tEBS Optimized: ", inst.ebsOptimized, "\n");
      print("End of instance\n\n");
    }
  }
}
