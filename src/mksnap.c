/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

int
eclat_create_snapshot(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct grecs_txtacc *acc = NULL;
	const char *descr = NULL;
	
	generic_proginfo->args_doc = "VOL-ID [DESCR]";
	generic_proginfo->docstring = "create a snapshot of an Amazon EBS volume";
	generic_parse_options(env->cmd,
			      "create a snapshot of an Amazon EBS volume",
			      argc, argv, &i);
	argc -= i;
	argv += i;
	if (argc < 1)
		die(EX_USAGE, "not enough arguments");
	translate_ids(1, argv, MAP_VOLUME);
	if (argc == 2)
		descr = argv[1];
	else if (argc > 2) {
		acc = grecs_txtacc_create();
		for (i = 1; i < argc; i++) {
			if (i > 1)
				grecs_txtacc_grow_char(acc, ' ');
			grecs_txtacc_grow(acc, argv[i], strlen(argv[i]));
		}
		grecs_txtacc_grow_char(acc, 0);
		descr = grecs_txtacc_finish(acc, 0);
	}

	eclat_request_add_param(env->request, "VolumeId", argv[0]);
	if (descr)
		eclat_request_add_param(env->request, "Description", descr);
	if (acc)
		grecs_txtacc_free(acc);
	return 0;
}

		

