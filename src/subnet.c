/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

#include "mksubnet-cl.h"

int
eclat_create_subnet(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;
	
	mksubnet_parse_options(env, argc, argv, &i);
	argc -= i;
	argv += i;
	if (argc != 2)
		die(EX_USAGE, "wrong number of arguments");

	eclat_request_add_param(q, "CidrBlock", argv[0]);
	eclat_request_add_param(q, "VpcId", argv[1]);
	if (zone)
		eclat_request_add_param(q, "AvailabilityZone", zone);
	return 0;
}

static char *subnet_states[] = {
	"pending",
	"available",
	NULL
};
static struct filter_descr filters[] = {
	{ "availabilityZone", FILTER_STRING },
	{ "availability-zone", FILTER_STRING },
	{ "available-ip-address-count", FILTER_INT },
	{ "cidrBlock", FILTER_STRING },
	{ "defaultForAz", FILTER_BOOL },
        { "default-for-az", FILTER_STRING },
	{ "state", FILTER_ENUM, subnet_states },
	{ "subnet-id", FILTER_STRING },
	{ "vpc-id", FILTER_STRING },
	{ "tag-key", FILTER_STRING },
	{ "tag-value", FILTER_STRING },
	{ "tag:<KEY>", FILTER_STRING },
	{ NULL }
};	

int
eclat_describe_subnets(eclat_command_env_t *env,
		       int argc, char **argv)
{
	int i;

	available_filters = filters;
	generic_proginfo->print_help_hook = list_filters;
	generic_proginfo->args_doc = "[FILTER...] [ID...]";
	generic_parse_options(env->cmd,
			      "describe subnets",
			      argc, argv, &i);
	argv += i;
	argc -= i;
	translate_ids(argc, argv, MAP_SUBNET);

	describe_request_create(env, argc, argv, "SubnetId");
	return 0;
}

int
eclat_modify_subnet_attribute(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;
	const char *attrname;
	char *bufptr = NULL;
	size_t bufsize = 0;
	static char *attrs[] = {
		"MapPublicIpOnLaunch"
	};
	
 	generic_proginfo->args_doc = "SUBNET-ID ATTR VALUE";
	available_attrs = attrs;
	generic_proginfo->print_help_hook = list_attrs;
	generic_parse_options(env->cmd,
			      "modify VPC attribute",
			      argc, argv, &i);
	argv += i;
	argc -= i;

	if (argc != 3)
		die(EX_USAGE, "wrong number of arguments");

	attrname = canonattrname(attrs, argv[1], NULL, NULL);
	if (!attrname)
		die(EX_USAGE, "unrecognized attribute name");

	translate_ids(1, argv, MAP_SUBNET);
	eclat_request_add_param(q, "SubnetId", argv[0]);
	grecs_asprintf(&bufptr, &bufsize, "%s.Value", attrname);
	eclat_request_add_param(q, bufptr, argv[2]);
	free(bufptr);
	return 0;
}

int
eclat_delete_subnet(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;

	generic_proginfo->args_doc = "SUBNET-ID";
	generic_parse_options(env->cmd,
			      "delete internet gateway",
			      argc, argv, &i);
	argv += i;
	argc -= i;

	if (argc != 1)
		die(EX_USAGE, "wrong number of arguments");

	translate_ids(1, argv, MAP_SUBNET);
	eclat_request_add_param(q, "SubnetId", argv[0]);

	return 0;
}
