/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

static void
parse_options(struct eclat_command const *cmd,
	      int argc, char *argv[], int *index)
{
	static struct filter_descr filters[] = {
		{ "endpoint",      FILTER_STRING },
		{ "region-name",   FILTER_STRING },
		{ NULL }
	};

	available_filters = filters;
	generic_proginfo->print_help_hook = list_filters;
	generic_proginfo->args_doc = "[REGION [REGION...]]";
	generic_parse_options(cmd,
			      "List available regions",
			      argc, argv, index);
}

int
eclat_describe_regions(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	
	parse_options(env->cmd, argc, argv, &i);
	argv += i;
	argc -= i;
	translate_ids(argc, argv, MAP_REG);

	describe_request_create(env, argc, argv, "RegionName");
	return 0;
}
