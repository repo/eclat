/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"
int force;
#include "devol-cl.h"

int
eclat_detach_volume(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;
	
	parse_options(env, argc, argv, &i);
	argc -= i;
	argv += i;

	if (argc < 1 || argc > 3)
		die(EX_USAGE, "bad number of arguments");

	translate_ids(1, argv, MAP_VOLUME);

	eclat_request_add_param(q, "VolumeId", argv[0]);
	if (argc > 1) {
		translate_ids(1, argv + 1, MAP_INSTANCE);
		eclat_request_add_param(q, "InstanceId", argv[1]);
		if (argc > 2)
			eclat_request_add_param(q, "Device", argv[2]);
	}
	if (force)
		eclat_request_add_param(q, "Force", "true");
	return 0;
}
