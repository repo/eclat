/* This file is part of Eclat.
   Copyright (C) 2015-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

static void
parse_options(struct eclat_command const *cmd, int argc, char *argv[],
	      int *index)
{
	static char *state_enum[] = { "pending", "available", NULL };
	static struct filter_descr filters[] = {
		{ "cidr", FILTER_STRING }, //FIXME: FILTER_CIDR
		{ "dhcp-options-id", FILTER_STRING },
		{ "isDefault", FILTER_BOOL },
		{ "state", FILTER_ENUM, state_enum },
		{ "tag-key", FILTER_STRING },
		{ "tag-value", FILTER_STRING },
		{ "tag:<KEY>", FILTER_STRING },
		{ "vpc-id", FILTER_STRING },
		{ NULL }
	};

	available_filters = filters;
	generic_proginfo->print_help_hook = list_filters;
	generic_proginfo->args_doc = _("[FILTER...] [VPC-ID...]");
	return generic_parse_options(cmd,
				     "List VPCs",
				     argc, argv, index);
}

int
eclat_describe_vpcs(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	
	parse_options(env->cmd, argc, argv, &i);
	argv += i;
	argc -= i;
	translate_ids(argc, argv, MAP_VPC);

	describe_request_create(env, argc, argv, "VpcId");
	return 0;
}


			
