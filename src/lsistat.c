/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"
static int all_option = 1;
#include "lsistat-cl.h"

int
eclat_describe_instance_status(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	
	parse_options(env, argc, argv, &i);
	argv += i;
	argc -= i;
	translate_ids(argc, argv, MAP_INSTANCE);
	
	describe_request_create(env, argc, argv, "InstanceId");

	if (all_option)
		eclat_request_add_param(env->request, "IncludeAllInstances", "1");
	return 0;
}
