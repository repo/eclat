/* This file is part of Eclat.
   Copyright (C) 2018-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

static char *modification_state_enum[] = {
	"modifying",
	"optimizing",
	"completed",
	"failed",
	NULL
};

static char *volume_type_enum[] = {
	"standard",
	"io1",
	"gp2",
	"sc1",
	"st1",
	NULL
};

static struct filter_descr filters[] = {
	{ "volume-id", FILTER_STRING },
	{ "modification-state", FILTER_ENUM, modification_state_enum },
	{ "target-size", FILTER_INT },
	{ "target-iops", FILTER_INT },
	{ "target-volume-type", FILTER_ENUM, volume_type_enum },
	{ "original-size", FILTER_INT },
	{ "original-iops", FILTER_INT },
	{ "original-volume-type", FILTER_ENUM, volume_type_enum },
	{ "start-time", FILTER_DATE },
	{ NULL }
};
	
int
eclat_describe_volumes_modifications(eclat_command_env_t *env, int argc,
				     char **argv)
{
	int i;

	available_filters = filters;
	generic_proginfo->print_help_hook = list_filters;
	generic_proginfo->args_doc = "[FILTER...] [ID...]";
	generic_parse_options(env->cmd,
			      "describe current modifications status of EBS volumes",
			      argc, argv, &i);
	argv += i;
	argc -= i;
	translate_ids(argc, argv, MAP_VOLUME);

	describe_request_create(env, argc, argv, "VolumeId");
	return 0;
}

