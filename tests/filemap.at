# This file is part of Eclat -*- Autotest -*-
# Copyright (C) 2012-2023 Sergey Poznyakoff
#
# Eclat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Eclat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Eclat.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([file])
AT_KEYWORDS([map filemap])

AT_DATA([test.conf],
[map file {
	type file;
	file "instances.txt";
}
])

AT_DATA([instances.txt],
[web:i-feed1234
dbserver:i-12345678
dbserv:i-deadbeef
])

AT_CHECK([eclat --config-file test.conf --test-map file dbserv],
[0],
[i-deadbeef
])

AT_CHECK([eclat --config-file test.conf --test-map file:rev i-feed1234],
[0],
[web
])

AT_CHECK([eclat --config-file test.conf --test-map file:rev i-12345678],
[0],
[dbserver
])

AT_CHECK([eclat --config-file test.conf --test-map file:rev i-deadbeef],
[0],
[dbserv
])

AT_CLEANUP
