/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <libeclat.h>
#include <gdbm.h>
#include <stdio.h>
#include <sysexits.h>
#include <string.h>

int
main(int argc, char **argv)
{
	GDBM_FILE db;
	int line = 0;
	char *dbname = NULL;
	int nulloption = 0;
	datum key, content;
	char buf[256];
	
	set_program_name(argv[0]);

	while (--argc) {
		char *arg = *++argv;
		if (arg[0] == '-') {
			if (strcmp(arg, "-0") == 0)
				nulloption = 1;
			else if (strcmp(arg, "-h") == 0) {
				printf("usage: %s [-i] dbname\n", program_name);
				return 0;
			} else
				die(EX_USAGE, "unknown option %s", arg);
		} else if (dbname)
			die(EX_USAGE, "database name given twice");
		else
			dbname = arg;
	}
	
	if (!dbname)
		die(EX_USAGE, "no database name");
	db = gdbm_open(dbname, 512, GDBM_NEWDB, 0600, NULL);
	if (!db)
		die(EX_SOFTWARE, "cannot open database: %s",
		    gdbm_strerror(gdbm_errno));
	while (fgets(buf, sizeof(buf), stdin)) {
		char *p;
		
		++line;
		eclat_trimnl(buf);
		p = strchr(buf, ':');
		if (!p) {
			err("%d: malformed input line", line);
			continue;
		}
		*p++ = 0;
		
		key.dptr = buf;
		key.dsize = strlen(key.dptr) + (nulloption ? 1 : 0);
		content.dptr = p;
		content.dsize = strlen(p);

		switch (gdbm_store(db, key, content, 0)) {
		case 0:
			break;
			
		case 1:
			err("%d: key alredy exists", line);
			continue;
      
		case -1:
			err("%d: %s", gdbm_strerror(gdbm_errno));
		}
	}
			
	gdbm_close(db);
	
	return 0;
}
