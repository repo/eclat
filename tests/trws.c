/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

/* This program filters out trailing whitespace from the input. */

#include <stdio.h>
#include <assert.h>

int
main()
{
	char buf[512];
	size_t lev = 0;
	int c;
	
	while ((c = getchar()) != EOF) {
		if (c == ' ' || c == '\t') {
			assert(lev < sizeof(buf));
			buf[lev++] = c;
			continue;
		} else if (c == '\n')
			lev = 0;
		else if (lev) {
			fwrite(buf, lev, 1, stdout);
			lev = 0;
		}
		putchar(c);
	}
	assert(!ferror(stdout));
	return 0;
}
			
				
