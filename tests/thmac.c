/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "libeclat.h"
#include "sha256.h"

char *progname;

static int
xdecode(const char *in, void **pout, size_t *outlen)
{
	unsigned char *out;
	size_t len;
	
	if (in[0] == '0' && in[1] == 'x') {
		size_t i, slen;
		unsigned char *p;
		
		slen = strlen(in);
		if (slen % 2)
			return -1;
		len = slen / 2 - 1; 
		out = malloc(len);
		if (!out) {
			perror("malloc");
			abort();
		}
		for (i = 2, p = out; i < slen; i+=2, p++) {
			static char xchar[] = "0123456789abcdef";
			char *q;
			unsigned char c;

			q = strchr(xchar, tolower(in[i]));
			if (q == 0)
				return i;
			c = (q - xchar);

			q = strchr(xchar, tolower(in[i+1]));
			if (q == 0)
				return i+1;
			*p = (c << 4) + (q - xchar);
		}
	} else {
		len = strlen(in);
		out = malloc(len);
		if (!out) {
			perror("malloc");
			abort();
		}
		memcpy(out, in, len);
	}
	*pout = out;
	*outlen = len;
	return 0;
}

void
xdecode_assert(int rc, char *in, char *what)
{
	switch (rc) {
	case -1:
		fprintf(stderr, "%s: odd %s length\n", progname, what);
		exit(1);

	case 0:
		break;

	default:
		fprintf(stderr, "%s: invalid hex character in %s near %s\n",
			progname, what, in + rc);
		exit(1);
	}
}

void
xdump(unsigned char *in, size_t len)
{
	printf("0x");
	while (len--)
		printf("%02x", *in++);
	printf("\n");
}

int
main(int argc, char **argv)
{
	int rc;
	void *key;
	size_t keylen;
	void *text;
	size_t textlen;
	unsigned char digest[SHA256_DIGEST_SIZE];
	
	progname = argv[0];
	if (argc != 3) {
		fprintf(stderr, "usage: %s key data\n", progname);
		return 1;
	}

	rc = xdecode(argv[1], &key, &keylen);
	xdecode_assert(rc, argv[1], "key");
	
	rc = xdecode(argv[2], &text, &textlen);
	xdecode_assert(rc, argv[2], "text");

	hmac_sha256(text, textlen, key, keylen, digest);

	xdump(digest, sizeof(digest));
	return 0;
}

		
