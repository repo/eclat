.\" This file is part of Eclat -*- nroff -*-
.\" Copyright (C) 2012-2023 Sergey Poznyakoff
.\"
.\" Eclat is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Eclat is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Eclat.  If not, see <http://www.gnu.org/licenses/>.
.TH ECLAT-MKINST 1 "November 17, 2018" "ECLAT" "Eclat User Reference"
.SH NAME
eclat-mkinst, eclat-run-instances \- launch new EC2 instances
.SH SYNOPSIS
.nh
.na
\fBeclat mkinst\fR [\fB\-a\fR \fIIFACE\fR] [\fB\-d\fR \fISTRING\fR]\
 [\fB\-f\fR \fI FILE\fR] [\fB\-g\fR \fISTRING\fR]\
 [\fB\-k\fR \fIID\fR] [\fB\-m\fR \fIDEV\fR=\fISPEC\fR] [\fB\-n\fR \fIN\fR]\
 [\fB\-p\fR \fINAME\fR] [\fB\-s\fR \fIID\fR] [\fB\-t\fR \fITYPE\fR]\
 [\fB\-z\fR \fIZONE\fR] [\fB\-\-data\fR=\fISTRING\fR]\
 [\fB\-\-data\-file\fR=\fIFILE\fR]\
 [\fB\-\-devmap\fR=\fIDEV\fR=\fISPEC\fR]\
 [\fB\-\-block\-device\-mapping\fR=\fIDEV\fR=\fISPEC\fR]\
 [\fB\-\-disable\-api\-termination\fR] [\fB\-\-ebs\-optimized\fR]\
 [\fB\-\-iam\-profile\fR=\fINAME\fR] [\fB\-\-kernel\fR=\fIID\fR]\
 [\fB\-\-keypair\fR=\fIID\fR] [\fB\-\-monitor\fR]\
 [\fB\-\-network\-interface\fR=\fIIFACE\fR] [\fB\-\-iface\fR=\fIIFACE\fR]\
 [\fB\-\-number\-secondary\-ip\fR=\fIN\fR]\
 [\fB\-\-secondary\-private\-ip\-address\-count\fR=\fIN\fR]\
 [\fB\-\-placement\-group\fR=\fINAME\fR]\
 [\fB\-\-private\-ip\-address\fR=\fIIP\fR]\
 [\fB\-\-ramdisk\fR=\fIID\fR]\
 [\fB\-\-secondary\-ip\fR=\fIIP\fR]\
 [\fB\-\-secondary\-private\-ip\-address\fR=\fIIP\fR]\
 [\fB\-\-security\-group\fR=\fISTRING\fR]\
 [\fB\-\-shutdown\fR=\fIstop|terminate\fR]\
 [\fB\-\-instance\-initiated\-shutdown\-behavior\fR=\fIstop|terminate\fR]\
 [\fB\-\-subnet\fR=\fIID\fR]\
 [\fB\-\-tenancy\fR=\fISTRING\fR]\
 [\fB\-\-type\fR=\fITYPE\fR] [\fB\-\-instance\-type\fR=\fITYPE\fR]\
 [\fB\-\-zone\fR=\fIZONE\fR]\
 [\fB\-\-availability\-zone\fR=\fIZONE\fR] \fIAMI-ID\fR
.PP
\fBeclat mkinst\fB \fB\-\-help\fR
.ad
.hy
.SH DESCRIPTION
The \fBmkinst\fR command launches one or more EC2 instances from an
existing AMI identified by the \fIAMI-ID\fR argument.  The number of
instances to launch is determined by the \fB\-n\fR option.  In its
absense, one instance will be launched.
.PP
If ID mapping is enabled (see the section \fBMAPS\fR
in
.BR eclat (1)),
the \fBImageId\fR map is used to translate \fIAMI-ID\fR to
the corresponding AMI identifier.
.PP
Options supply additional details for the instances to be created.
.SH OPTIONS
.TP
\fB\-a\fR, \fB\-\-network\-interface\fR, \fB\-\-iface\fR=\fIIFACE\fR
Specify the network attachment for a VPC instance.
.TP
\fB\-d\fR, \fB\-\-data\fR=\fBSTRING\fR
User data to pass to the instance.
.TP
\fB\-\-disable\-api\-termination\fR
Disable API termination.
.TP
\fB\-\-ebs\-optimized\fR
Optimize the instances for EBS I/O.
.TP
\fB\-f\fR, \fB\-\-data\-file\fR=\fIFILE\fR
Read user data from \fIFILE\fR.
.TP
\fB\-g\fR, \fB\-\-security\-group\fR=\fISTRING\fR
The name of the security group.
.TP
\fB\-k\fR, \fB\-\-keypair\fR=\fIID\fR
Key pair name.
.TP
\fB\-\-kernel\fR=fIID\fR
Select kernel ID.
.TP
\fB\-m\fR, \fB\-\-devmap\fR, \fB\-\-block\-device\-mapping\fR=\fIDEV\fR=\fISPEC\fR
Set block device mapping.  The \fIDEV\fR part is the device name of the physical device on
the instance to map.  The \fISPEC\fR sets the mapping specification
and can be one of:
.RS 7
.TP
.B none
Disable the existing mapping.  E.g. \fB\-m /dev/xvdb=none\fR disables
the mapping for the device \fB/dev/xvdb\fR.
.TP
.BR ephemeral [0-3]
Map an instance store volume to the device.  E.g. \fB\-m
/dev/xvdb=ephemeral1\fR.
.TP
[\fIsnapshot\-id\fR]:\
[\fIvolume\-size\fR]:\
[\fIDOT\fR]:\
[\fIstandard\fR|\fIio1\fR[:\fIiops\fR]]
Specifies an EBS volume to be mapped to the device.  The
\fIsnapshot\-id\fR specifies the ID of the snapshot to create the
volume from, \fIvolume\-size\fR gives the desired size of the volume
in gigabytes.  The \fIDOT\fR parameter (\fBdelete on termination\fR)
specifies whether the device should be deleted after the instance is
terminated.  Possible values are \fBtrue\fR and \fBfalse\fR.  The last
parameter defines the volume type.  The value \fBstandard\fR instructs
Amazon to create a standard volume.  This is the default.  To create a
provisioned IOPS volume, specify \fBio1\fR.  In that case you can also
supply the number of IOPS that the volume supports in the 
\fIiops\fR argument.  For example, \fB\-m
/dev/xvdf=snap-12345abc::false:io1:500\fR.
.RE
.TP
\fB\-\-monitor\fR
Enable monitoring.
.TP
\fB\-n\fR \fIN\fR
Number of instances to launch.
.TP
\fB\-\-number\-secondary\-ip\fR, \fB\-\-secondary\-private\-ip\-address\-count\fR=\fIN\fR
Number of secondary IP addresses to assign.
.TP
\fB\-p\fR, \fB\-\-iam\-profile\fR=\fINAME\fR
\fBIAM\fR instance profile to associate with the launched instances.
.TP
\fB\-\-placement\-group\fR=\fINAME\fR
Name of the placement group.
.TP
\fB\-\-private\-ip\-address\fR=\fIIP\fR
Assign a specific private IP to the VPC instance.
.TP
\fB\-\-ramdisk\fR=\fIID\fR
Select ramdisk ID.
.TP
\fB\-s\fR, \fB\-\-subnet\fR=\fIID\fR
Subnet to launch the instance into.
.TP
\fB\-\-secondary\-ip\fR, \fB\-\-secondary\-private\-ip\-address\fR=\fIIP\fR
Assign the IP as a secondary private IP address.
.TP
\fB\-\-shutdown\fR, \fB\-\-instance\-initiated\-shutdown\-behavior\fR=\fBstop\fR|\fBterminate\fR
What to do on instance shutdown.
.TP
\fB\-t\fR, \fB\-\-type\fR, \fB\-\-instance\-type\fR=\fITYPE\fR
Instance type.
.TP
\fB\-\-tenancy\fR=\fISTRING\fR
Placement tenancy.
.TP
\fB\-z\fR, \fB\-\-zone\fR, \fB\-\-availability\-zone\fR=\fIZONE\fR
Set availablility zone.
.SH OUTPUT
On success, the command outputs detailed description of each created
instance in the same format as the
.BR eclat-lsinst (1)
command
.SH "SEE ALSO"
.BR eclat (1),
.BR eclat\-terminate (1),
.BR eclat\-lsiattr (1),
.BR eclat\-lsinst (1),
.BR eclat\-lsistat (1),
.BR eclat\-reboot (1),
.BR eclat\-setiattr (1),
.BR eclat\-start (1),
.BR eclat\-stop (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <bug\-eclat@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2012-2018 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

