.\" This file is part of Eclat -*- nroff -*-
.\" Copyright (C) 2012-2023 Sergey Poznyakoff
.\"
.\" Eclat is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Eclat is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Eclat.  If not, see <http://www.gnu.org/licenses/>.
.TH ECLAT-LSISTAT 1 "January 26, 2015" "ECLAT" "Eclat User Reference"
.SH NAME
eclat-lsistat, eclat-describe-instance\-status \- describe status of EC2 instances
.SH SYNOPSIS
\fBeclat\fR\
 \fBlsistat\fR
 [\fB\-ar\fR]\
 [\fB\-\-all\fR]\
 [\fB\-\-running\fR]\
 [\fIFILTER\fR...]
.PP
\fBeclat lsistat\fR \fB\-\-help\fR
.SH DESCRIPTION
Describes the status of selected EC2 instances including any scheduled
events.  This information includes:
.nr step 1 1
.IP \n[step].
.B Instance state

This describes the current state of the instance: \fBrunning\fR,
\fBstopped\fR, etc.
.IP \n+[step].
.B System status

The system status refers to the functionality depending on the systems
that support the instance, such as the underlying hardware, network
devices, etc.
.IP \n+[step].
.B Instance status

The instance status describes those aspects of the functionality that
depend on the instance itself (e.g. on its operating system, the
services it runs, etc.)
.SH OPTIONS
.TP
\fB\-a\fR, \fB\-\-all\fR
Return the status for all instances.  This is the default.
.TP
\fB\-r\fR, \fB\-\-running\fR
Show only running instances.
.SH FILTERS
The following filters are defined:
.TP
\fBavailability\-zone\fR=\fIstring\fR
The Availability Zone of the instance.
.TP
\fBevent.code\fR=\fIvalue\fR
The code identifying the type of event.  Valid \fIvalue\fRs are: 
.BR instance\-reboot ,
.BR system\-reboot ,
.BR system\-maintenance ,
.BR instance\-retirement .
.TP
\fBevent.description\fR=\fIstring\fR
A description of the event.
.TP
\fBevent.not\-after\fR=\fIdate\fR
The latest end time for the scheduled event.
.TP
\fBevent.not\-before\fR=\fIdate\fR
The earliest start time for the scheduled event.
.TP
\fBinstance\-state\-name\fR=\fIvalue\fR
The state of the instance.  The valid \fIvalue\fRs are:
.BR pending ,
.BR running ,
.BR shutting\-down ,
.BR terminated ,
.BR stopping ,
.BR stopped .

Note that unless \fIvalue\fR is \fBrunning\fR you have to use the
\fB\-a\fR (\fB\-\-all\fR) option for this filter to take effect.
.TP
\fBinstance\-state\-code\fR=\fIcode\fR
A code representing the state of the instance.
Valid values are:
.RS
.TP
.B 0
pending
.TP
.B 16
running
.TP
.B 32
shutting\-down
.TP
.B 48
terminated
.TP
.B 64
stopping
.TP
.B 80
stopped
.RE
.sp
Note that unless \fIcode\fR is \fB0\fR you have to use the
\fB\-a\fR (\fB\-\-all\fR) option for this filter to take effect.
.TP
\fBsystem\-status.status\fR=\fIvalue\fR
The system status of the instance.  Valid values are:
.BR ok ,
.BR impaired ,
.BR initializing ,
.BR insufficient\-data ,
.BR not\-applicable .
.TP
\fBsystem\-status.reachability\fR=\fIstring\fR
System reachability status, one of:
.BR passed ,
.BR failed ,
.BR initializing ,
.BR insufficient\-data .
.TP
\fBinstance\-status.status\fR=\fIstring\fR
The status of the instance.  One of:
.BR ok ,
.BR impaired ,
.BR initializing ,
.BR insufficient\-data ,
.BR not\-applicable .
.TP
\fBinstance\-status.reachability\fR=\fIstring\fR
The instance reachability status.  One of:
.BR passed ,
.BR failed ,
.BR initializing ,
.BR insufficient\-data .
.SH OUTPUT
Each instance is described on a separate line.  The fields are
separated by single \fBTAB\fR characters.  The fields displayed are
(from left to right): instance ID, region, and instance state.  If the
instance is running, system and instance status are also displayed.
If any events are reported, they are output on the standard error.
.SH EXAMPLE
In the example below the long lines are split for readability:
.PP
.EX
$ eclat lsistat \-a
i\-283f9f47 us\-east\-1d running impaired\
 Sys.reachability=failed Inst.reachability=failed
i\-283f9f47	Event: "The instance is running on degraded hardware"
i\-d2e36dbd us\-east\-1d running ok\
  Sys.reachability=passed	Inst.reachability=passed
.EE
.SH "SEE ALSO"
.BR eclat (1),
.BR eclat\-lsinst (1),
.BR eclat\-lsiattr (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <bug\-eclat@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2012-2018 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

