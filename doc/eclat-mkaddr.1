.\" This file is part of Eclat -*- nroff -*-
.\" Copyright (C) 2012-2023 Sergey Poznyakoff
.\"
.\" Eclat is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Eclat is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Eclat.  If not, see <http://www.gnu.org/licenses/>.
.TH ECLAT-MKADDR 1 "January 28, 2015" "ECLAT" "Eclat User Reference"
.SH NAME
eclat-mkaddr, eclat-allocate-address \- allocate an Elastic IP address
.SH SYNOPSIS
\fBeclat mkaddr\fR [\fB\-v\fR] [\fB\-\-vpc\fR] 
.PP
\fBeclat mkaddr\fR \fB\-\-help\fR
.SH DESCRIPTION
The
.B mkaddr
command allocates an Elastic IP address.  It does not take arguments.
.PP
.SH OPTIONS
.TP
\fB\-v\fR, \fB\-\-vpc\fR
Allocate address for use with instances in a VPC.
.SH "OUTPUT AND RETURN VALUE"
The default output format displays the allocated IP address on
success.
.PP
On success, the code 0 is returned to the shell.  Non-zero status code
is returned on error.
.SH "SEE ALSO"
.BR eclat (1),
.BR eclat\-assocaddr (1),
.BR eclat\-rmaddr (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <bug\-eclat@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2012-2018 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

