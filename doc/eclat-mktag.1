.\" This file is part of Eclat -*- nroff -*-
.\" Copyright (C) 2012-2023 Sergey Poznyakoff
.\"
.\" Eclat is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Eclat is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Eclat.  If not, see <http://www.gnu.org/licenses/>.
.TH ECLAT-MKTAG 1 "January 26, 2015" "ECLAT" "Eclat User Reference"
.SH NAME
eclat-mktag, eclat-create-tags \- create or replace tags for a resource
.SH SYNOPSIS
.nh
.na
\fBeclat mktag\fR [\fB\-a\fR \fIID\fR] [\fB\-i\fR \fIID\fR]\
 [\fB\-r\fR [\fIMAP\fR:]\fIID\fR] [\fB\-s \fIID\fR] [\fB\-v\fR \fIID\fR]\
 [\fB\-T\fR \fIFILE\fR]\
 [\fB\-\-ami\fR \fIID\fR] [\fB\-\-instance\fR \fIID\fR]\
 [\fB\-\-resource\-id\fR [\fIMAP\fR:]\fIID\fR]\
 [\fB\-\-volume\fR \fIID\fR] [\fB\-\-snapshot\fR \fIID\fR]\
 [\fB\-\-from\-file\fR \fIFILE\fR] [\fIID\fR]... \fITAG\fR[=\fIVAL\fR]...
.PP
\fBeclat mktag\fR \fB\-\-help\fR
.ad
.hy
.SH DESCRIPTION
This command creates or replaces one or more tags for given
resources.  Tags are specified by their names.  If a tag name is
followed by an equals sign, the characters following that sign are
stored in this tag's value.  Otherwise an empty value is assigned to
it.
.PP
The resources to create the tags for are supplied with one of the
options.  At least one resource designation must be present.
.PP
If ID mapping is enabled (see the section \fBMAPS\fR
in
.BR eclat (1)),
all options except \fB\-r\fR (\fB\-\-resource\-id\fR) translate their
arguments using corresponding maps (see
.B OPTIONS
below).  The \fB\-r\fR (\fB\-\-resource\-id\fR) option uses no map by
default, unless the map name is specified explicitly before the ID.
.SH OPTIONS
.TP
\fB\-a\fR, \fB\-\-ami\fR \fIID\fR
Creates tags for the given image.  In resource translation mode, uses
the map \fBImageId\fR.
.TP
\fB\-i\fR, \fB\-\-instance\fR \fIID\fR
Creates tags for the given instance.  In resource translation mode, uses
the map \fBInstanceId\fR.
.TP
\fB\-r\fR, \fB\-\-resource\-id\fR [\fIMAP\fR:]\fIID\fR
Creates tags for the EC2 resource identified by \fIID\fR.  If optional
\fIMAP\fR prefix is supplied and resource translation mode is enabled,
this option will use the \fIMAP\fR to translate \fIID\fR into AWS
resource identifier.
.TP
\fB\-s\fR, \fB\-\-snapshot\fR \fIID\fR
Creates tags for the given snapshot.  In resource translation mode, uses
the map \fBSnapshotId\fR.
.TP
\fB\-T\fR, \fB\-\-from\-file\fR \fIFILE\fR
Reads tags from \fIFILE\fR or from the standard input, if \fIFILE\fR
is \fB\-\fR.  Empty lines and lines beginning with \fB#\fR are
ignored.  Non-empty lines must contain either a tag name alone, or a
tag name and the corresponding value, separated by an equals sign.  If
the value is enclosed in double quotes, these will be removed and any
escaped characters within the value translated to their corresponding
equivalents using shell rules (e.g. \fB\\\(dq\fR becomes \fB\(dq\fR,
etc).  See example 2 below.
.TP
\fB\-v\fR, \fB\-\-volume\fR \fIID\fR
Creates tags for the given volume.  In resource translation mode, uses
the map \fBVolumeId\fR.
.SH OUTPUT
By default the command does not output anything, unless an error
occurs in which case the error diagnostics is printed on the standard
error.
.SH EXAMPLE
.nr step 1 1
.IP \n[step].
Create two tags for an instance:
.sp
.EX
eclat mktag \-i i\-12345678 Name=Webserver test
.EE
.sp
This command creates two tags for instance \fBi\-12345678\fR: the tag
\fBName\fR with the value \fBWebserver\fR and tag \fBtest\fR with
empty value.
.IP \n+[step].
Copy tags between instances:
.sp
.EX
eclat -e 'if (.DescribeTagsResponse)
            for (var in .DescribeTagsResponse.tagSet.item)
              print(var.key,"=",var.value,"\\n");' \\
      lstag resource\-id=i\-12345678 |
 eclat mktag \-i i\-fe2345ed \-T \-
.EE
.sp
This example uses the
.B lstag
command to obtain tags from the instance
.B i-12345678
and a custom format to represent them in a form suitable as input
to
.BR mktag .
The obtained data are piped to the
.B mktag
command which is instructed to read them from the standard input using
.BR "\-T \-" .
.SH "SEE ALSO"
.BR eclat (1),
.BR eclat\-rmtag (1),
.BR eclat\-lstag (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <bug\-eclat@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2012-2018 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

