# This file is part of Eclat -*- Makefile -*-
# Copyright (C) 2012-2023 Sergey Poznyakoff
#
# Eclat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Eclat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Eclat.  If not, see <http://www.gnu.org/licenses/>.

# This file contains the lists of manual pages to be installed.  They
# are kept separately so they may be included from another makefiles.
# Currently they are used in the Makefile.am and in the webdoc Makefile.

MANPAGES1=\
 eclat.1\
 eclat-addr2ec2.1\
 eclat-addr2vpc.1\
 eclat-assocaddr.1\
 eclat-atvol.1\
 eclat-chvol.1\
 eclat-clrsattr.1\
 eclat-dmesg.1\
 eclat-cpimg.1\
 eclat-cpsnap.1\
 eclat-deimg.1\
 eclat-devol.1\
 eclat-disasaddr.1\
 eclat-lsaddr.1\
 eclat-lsaattr.1\
 eclat-lschvol.1\
 eclat-lsiattr.1\
 eclat-lsimg.1\
 eclat-lsinst.1\
 eclat-lsistat.1\
 eclat-lsreg.1\
 eclat-lssattr.1\
 eclat-lssg.1\
 eclat-lssnap.1\
 eclat-lstag.1\
 eclat-lsvol.1\
 eclat-lszon.1\
 eclat-mkaddr.1\
 eclat-mkimg.1\
 eclat-mkinst.1\
 eclat-mksg.1\
 eclat-mksnap.1\
 eclat-mktag.1\
 eclat-mkvol.1\
 eclat-mkvpc.1\
 eclat-reboot.1\
 eclat-rmaddr.1\
 eclat-rmsg.1\
 eclat-rmsnap.1\
 eclat-rmtag.1\
 eclat-setaattr.1\
 eclat-setiattr.1\
 eclat-setsattr.1\
 eclat-sg.1\
 eclat-start.1\
 eclat-stop.1\
 eclat-terminate.1\
 ispeek.1

MANPAGES5=\
 eclat.conf.5
