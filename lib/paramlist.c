/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <string.h>
#include "libeclat.h"
#include "grecs.h"

struct grecs_list *
ec2_param_list_create()
{
	struct grecs_list *lp = grecs_list_create();
	lp->free_entry = ec2_param_free;
	return lp;
}

void
ec2_param_list_append(struct grecs_list **lpp, char const *name,
		      char const *val)
{
	struct ec2_param *p;
	if (!*lpp)
		*lpp = ec2_param_list_create();
	p = grecs_zalloc(sizeof(*p));
	p->name = grecs_strdup(name);
	p->value = grecs_strdup(val);
	p->encoded = 0;
	grecs_list_append(*lpp, p);
}

void
eclat_request_add_param_list(struct ec2_request *req, struct grecs_list *lp)
{
	struct grecs_list_entry *ep;

	if (!lp)
		return;
	for (ep = lp->head; ep; ep = ep->next) {
		struct ec2_param *param = ep->data;
		eclat_request_add_param0(req,
					 param->name, param->value, 
					 param->encoded);
	}
}
