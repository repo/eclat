/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "libeclat.h"
#include <string.h>

char *
path_concat(const char *dir, const char *comp)
{
	char *p;
	size_t len = strlen(dir);

	while (len > 0 && dir[len-1] == '/')
		--len;

	while (*comp == '/')
		++comp;
	
	p = grecs_malloc(len + strlen(comp) + 2);
	memcpy(p, dir, len);
	p[len++] = '/';
	strcpy(p + len, comp);
	return p;
}
